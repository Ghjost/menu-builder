<?php

namespace App\Services\MenuBuilder\Services;

use App\Services\MenuBuilder\Menu\Menu;

Interface MenuBuilderInterface
{
    public function buildMenu () : Menu;

    public function addTopItem (
        string $name,
        string $pathName,
        ?string $icon = null
    ) : void;
    public function addTopSeparator () : void;
    public function addTopDropDown (
        string $name,
        ?string $icon = null,
        array $element
    ) : void;

    public function addBottomItem (
        string $name,
        string $pathName,
        ?string $icon = null
    ) : void;
    public function addBottomSeparator () : void;
    public function addBottomDropDown (
        string $name,
        ?string $icon = null,
        array $element
    ) : void;

}