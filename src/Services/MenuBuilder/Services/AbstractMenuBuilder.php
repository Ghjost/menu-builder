<?php

namespace App\Services\MenuBuilder\Services;

use App\Services\MenuBuilder\Menu\Menu;
use App\Services\MenuBuilder\MenuElement\AbstractMenuElement;
use App\Services\MenuBuilder\MenuElement\MenuElementDropDown;
use App\Services\MenuBuilder\MenuElement\MenuElementItem;
use App\Services\MenuBuilder\MenuElement\MenuElementSeparator;
use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractMenuBuilder implements MenuBuilderInterface
{
    public Menu $menu;

    public function buildMenu(): Menu
    {
        $this->menu = new Menu();

        return $this->menu;
    }

    public function addTopItem(
        string  $name,
        string  $pathName,
        ?string $icon = null
    ): void
    {
        $this->menu->addTopElement(new MenuElementItem($name, $pathName, $icon));
    }

    public function addTopSeparator(): void
    {
        $this->menu->addTopElement(new MenuElementSeparator());
    }

    public function addTopDropDown(
        string $name,
        ?string $icon = null,
        array $subitems
    ): void
    {
        $subItemList = new ArrayCollection();

        foreach ($subitems as $item) {
            $subItemList->add(new MenuElementItem($item[0], $item[1], $item[2] ));
        }

        $this->menu->addTopElement(new MenuElementDropDown(
            $name,
            $icon,
            $subItemList
        ));
    }

    public function addBottomItem(
        string  $name,
        string  $pathName,
        ?string $icon = null
    ): void
    {
        $this->menu->addBottomElement(new MenuElementItem($name, $pathName, $icon));
    }

    public function addBottomSeparator(): void
    {
        $this->menu->addBottomElement(new MenuElementSeparator());
    }

    public function addBottomDropDown(
        string $name,
        ?string $icon = null,
        array $subitems
    ): void
    {
        $subItemList = new ArrayCollection();

        foreach ($subitems as $item) {
            $subItemList->add(new MenuElementItem($item[0], $item[1], $item[2] ));
        }

        $this->menu->addBottomElement(new MenuElementDropDown(
            $name,
            $icon,
            $subItemList
        ));
    }
}