<?php

namespace App\Services\MenuBuilder\Services;

use App\Services\MenuBuilder\Menu\Menu;

class MenuBuilder extends AbstractMenuBuilder
{

    public function buildMenu(): Menu
    {
        $this->menu = new Menu();
        $this->menu->setExpandable(true);
        $this->addTopItem('Menu 1', 'app_dashboard', 'home-outline');
        $this->addTopItem('Menu 2', 'app_dashboard1', 'airplane-outline');
        $this->addTopSeparator();
        $this->addTopItem('Menu 3', 'app_dashboard2', 'baseball-outline');
        $this->addTopSeparator();
        $this->addTopDropDown(
            'dropdown',
            'alarm-outline',
            [
                [
                    'Sub menu 1','submenu_1', "earth-outline"
                ],
                [
                    'Sub menu 2','submenu_2', "earth-outline"
                ]
            ]
        );

        $this->addTopDropDown(
            'dropdown 2',
            'alarm-outline',
            [
                [
                    'Sub menu 3','submenu_3', "earth-outline"
                ],
                [
                    'Sub menu 4','submenu_4', "earth-outline"
                ]
            ]
        );
        $this->addTopItem('Menu 4', 'app_dashboard3', 'earth-outline');

        $this->addBottomItem('Login', 'login', 'keypad-outline');
        $this->addBottomItem('Logout', 'logout', 'keypad-outline');

        return $this->menu;
    }

//    public function buildMenu (): Menu
//    {
//
//        $menu = new Menu();
//
//        $menu->setTopElement(
//            new ArrayCollection([
//                new MenuElementItem('Menu', 'app_dashboard', 'home-outline'),
//                new MenuElementItem('Menu 1', 'app_dashboard1', 'airplane-outline'),
//                new MenuElementSeparator(),
//                new MenuElementDropDown('Menu 2 Drop', 'home-outline', new ArrayCollection([
//                    new MenuElementItem('Menu', 'app_dashboard', 'home-outline'),
//                    new MenuElementItem('Menu 1', 'app_dashboard1', 'airplane-outline'),
//                ])),
//                new MenuElementItem('Menu 2', 'app_dashboard2', 'earth-outline'),
//                new MenuElementSeparator(),
//                new MenuElementItem('Menu 3', 'app_dashboard3', 'game-controller-outline'),
//                new MenuElementSeparator(),
//                new MenuElementDropDown('Menu 1 Drop', 'home-outline', new ArrayCollection([
//                    new MenuElementItem('Menu', 'app_dashboard', 'home-outline'),
//                    new MenuElementItem('Menu 1', 'app_dashboard1', 'airplane-outline'),
//                ]))
//            ])
//        );
//
//        $menu->setBottomElement(
//            new ArrayCollection([
//                new MenuElementItem('Menu', 'app_dashboard', 'home-outline'),
//                new MenuElementItem('Menu 1', 'app_dashboard1', 'airplane-outline'),
//                new MenuElementSeparator(),
//                new MenuElementDropDown('Menu 2 Drop', 'home-outline', new ArrayCollection([
//                    new MenuElementItem('Menu', 'app_dashboard', 'home-outline'),
//                    new MenuElementItem('Menu 1', 'app_dashboard1', 'airplane-outline'),
//                ])),
//                new MenuElementItem('Menu 2', 'app_dashboard2', 'earth-outline'),
//                new MenuElementSeparator(),
//                new MenuElementItem('Menu 3', 'app_dashboard3', 'game-controller-outline'),
//                new MenuElementSeparator(),
//                new MenuElementDropDown('Menu 1 Drop', 'home-outline', new ArrayCollection([
//                    new MenuElementItem('Menu', 'app_dashboard', 'home-outline'),
//                    new MenuElementItem('Menu 1', 'app_dashboard1', 'airplane-outline'),
//                ]))
//            ])
//        );

//        return $menu;
//    }

}