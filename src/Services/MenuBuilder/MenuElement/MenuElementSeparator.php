<?php

namespace App\Services\MenuBuilder\MenuElement;

class MenuElementSeparator extends AbstractMenuElement
{
    public function __construct(
    )
    {
        parent::__construct(AbstractMenuElement::TYPE_SEPARATOR);

    }
}