<?php

namespace App\Services\MenuBuilder\MenuElement;

abstract class AbstractMenuElement
{
    public const TYPE_ITEM = "ITEM";
    public const TYPE_DROPDOWN = "DROPDOWN";
    public const TYPE_SEPARATOR = "SEPARATOR";

    public const TYPE_OPTIONS = "OPTIONS";

    private string $id;
    private string $type;

    public function __construct(
        string $type
    )
    {
        $this->type = $type;
        $this->id = uniqid('menu_builder_id');
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }
}