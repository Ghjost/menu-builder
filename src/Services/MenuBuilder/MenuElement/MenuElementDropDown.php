<?php

namespace App\Services\MenuBuilder\MenuElement;

use Doctrine\Common\Collections\ArrayCollection;

class MenuElementDropDown extends AbstractMenuElement
{
    private string $name;

    private string $pathName;

    private ?string $icon;

    private ArrayCollection $subitems;

    public function __construct(
        string $name,
        ?string $icon = null,
        ArrayCollection $subitems,
    )
    {
        parent::__construct(AbstractMenuElement::TYPE_DROPDOWN);
        $this->name = $name;
        $this->icon = $icon;
        $this->subitems = $subitems;

    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPathName(): string
    {
        return $this->pathName;
    }

    public function setPathName(string $pathName): void
    {
        $this->pathName = $pathName;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }

    public function getSubitems(): ArrayCollection
    {
        return $this->subitems;
    }

    public function setSubitems(ArrayCollection $subitems): void
    {
        $this->subitems = $subitems;
    }
}