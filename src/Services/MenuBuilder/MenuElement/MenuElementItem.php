<?php

namespace App\Services\MenuBuilder\MenuElement;

class MenuElementItem extends AbstractMenuElement
{
    private string $name;

    private string $pathName;

    private ?string $icon;

    public function __construct(
        string $name,
        string $pathName,
        ?string $icon = null
    )
    {
        parent::__construct(AbstractMenuElement::TYPE_ITEM);
        $this->name = $name;
        $this->pathName = $pathName;
        $this->icon = $icon;

    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }

    public function getPathName(): string
    {
        return $this->pathName;
    }

    public function setPathName(string $pathName): void
    {
        $this->pathName = $pathName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }


}