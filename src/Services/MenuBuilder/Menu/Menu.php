<?php

namespace App\Services\MenuBuilder\Menu;

use App\Services\MenuBuilder\MenuElement\AbstractMenuElement;
use Doctrine\Common\Collections\ArrayCollection;

class Menu extends AbstractMenu
{
    private bool $logoDisplayed = false;
    private string $logo = "";
    private bool $expandable = true;

    private ArrayCollection $topElement;
    private ArrayCollection $bottomElement;

    public function __construct (
    ) {
        $this->topElement = new ArrayCollection();
        $this->bottomElement = new ArrayCollection();
    }

    public function isExpandable(): bool
    {
        return $this->expandable;
    }

    public function setExpandable(bool $expandable): void
    {
        $this->expandable = $expandable;
    }

    public function getTopElement(): ArrayCollection
    {
        return $this->topElement;
    }

    public function setTopElement(ArrayCollection $topElement): void
    {
        $this->topElement = $topElement;
    }

    public function addTopElement(AbstractMenuElement $element): Menu
    {
        $this->topElement->add($element);
        return $this;
    }

    public function getBottomElement(): ArrayCollection
    {
        return $this->bottomElement;
    }

    public function addBottomElement(AbstractMenuElement $element): Menu
    {
        $this->bottomElement->add($element);
        return $this;
    }

    public function setBottomElement(ArrayCollection $bottomElement): void
    {
        $this->bottomElement = $bottomElement;
    }
}