<?php

namespace App\Controller;

use App\Services\MenuBuilder\Services\MenuBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    #[Route('/', name: 'app_dashboard', defaults: ['active_menu' => 'app_dashboard'])]
    #[Route('/1', name: 'app_dashboard1', defaults: ['active_menu' => 'app_dashboard1'])]
    #[Route('/2', name: 'app_dashboard2', defaults: ['active_menu' => 'app_dashboard2'])]
    #[Route('/3', name: 'app_dashboard3', defaults: ['active_menu' => 'app_dashboard3'])]
    #[Route('/4', name: 'app_dashboard4', defaults: ['active_menu' => 'app_dashboard4'])]
    #[Route('/login', name: 'login', defaults: ['active_menu' => 'login'])]
    #[Route('/logout', name: 'logout', defaults: ['active_menu' => 'logout'])]
    #[Route('/submenu_1', name: 'submenu_1', defaults: ['active_menu' => 'submenu_1'])]
    #[Route('/submenu_2', name: 'submenu_2', defaults: ['active_menu' => 'submenu_2'])]
    #[Route('/submenu_3', name: 'submenu_3', defaults: ['active_menu' => 'submenu_3'])]
    #[Route('/submenu_4', name: 'submenu_4', defaults: ['active_menu' => 'submenu_4'])]
    public function index(MenuBuilder $menuBuilderSecond): Response
    {
        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
//            'menu_builder' => $menuBuilder->buildMenu() ,
            'menu_builder2' => $menuBuilderSecond->buildMenu()
        ]);
    }

    #[Route('/json', name: 'json', defaults: ['active_menu' => 'json'])]
    public function indexJson(): JsonResponse
    {
        return new JsonResponse([
            'controller_name' => 'DashboardController',
        ]);
    }

}
