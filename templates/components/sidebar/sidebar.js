// {# https://stackoverflow.com/questions/22222810/disable-css-transitions-on-page-load-only #}
// console.log('Sidebar file loaded')

const sidebar = document.getElementById('sidebar');
const expandSideBarChechbox = document.getElementById('sidebar-expand-toogle');

if(localStorage.getItem("sidebarStyle")) {
    sidebar.className = ""
    sidebar.classList.add(localStorage.getItem("sidebarStyle"))
}else{
    sidebar.classList.add('sidebar')
}
expandSideBarChechbox.addEventListener('change', (event) => {
    const isChecked = event.target['checked']

    console.log(isChecked)
    sidebar.classList.toggle('sidebar')
    sidebar.classList.toggle('sidebar-low-profile')

    if(sidebar.classList.contains('sidebar')) {
        localStorage.setItem("sidebarStyle", "sidebar")
    } else if(sidebar.classList.contains('sidebar-low-profile')) {
        localStorage.setItem("sidebarStyle", "sidebar-low-profile")
    }
})

const dropdownList = document.querySelectorAll('[data-toogler]')
console.log(dropdownList);

dropdownList.forEach((dropdownItem) => {

    let parentContainer = dropdownItem.parentNode
    if (dropdownItem.checked) {
        parentContainer.classList.add('active')
    }

    dropdownItem.addEventListener('click', (event) => {
        dropdownList.forEach((dropdownItemTemp) => {
            const parentContainerTemp = dropdownItemTemp.parentNode;
            if (parentContainerTemp !== parentContainer) {
                parentContainerTemp.classList.remove('active')
            }
        })
        parentContainer.classList.toggle("active")
    })
})
